package com.example.fpapara.themoviedb;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import com.example.fpapara.themoviedb.ui.movielist.MovieListActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class ApplicationTestShowToast extends ActivityInstrumentationTestCase2<MovieListActivity> {
    @Rule
    public ActivityTestRule<MovieListActivity> mActivityRule =
            new ActivityTestRule<>(MovieListActivity.class);

    public ApplicationTestShowToast() {
        super(MovieListActivity.class);
    }


    @Test
    public void test_err_failed_to_load_movies() {
        onView(withText(R.string.err_failed_to_load))
                .inRoot(new ToastMatcher()).check(matches(not(isDisplayed())));
    }
}
