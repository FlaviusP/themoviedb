package com.example.fpapara.themoviedb;

import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.fpapara.themoviedb.ui.movielist.MovieListActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class ApplicationTest {
    @Rule
    public ActivityTestRule<MovieListActivity> mActivityRule =
            new ActivityTestRule<>(MovieListActivity.class);


    @Test
    public void testSwipeViewPagerAndChangeTab() {
        SystemClock.sleep(500);
        onView(withId(R.id.viewpager)).perform(swipeLeft());
        SystemClock.sleep(300);
        onView(withId(R.id.viewpager)).perform(swipeRight());
    }

    @Test
    public void testScroll() {
        SystemClock.sleep(500);
        onView(allOf(withId(R.id.movieRecyclerView), withParent(withId(R.id.movieRecyclerView + 1000))))
                .perform(RecyclerViewActions.scrollToPosition(5));
        SystemClock.sleep(500);
        onView(allOf(withId(R.id.movieRecyclerView), withParent(withId(R.id.movieRecyclerView + 1000))))
                .perform(RecyclerViewActions.scrollToPosition(18), click());
        SystemClock.sleep(1000);
        onView(withText(R.string.home_page)).check(matches(isDisplayed()));
    }

    @Test
    public void testClickOnTabs() {
        onView(withText(R.string.movie_upcoming)).perform(click());
        onView(withText(R.string.movie_popular)).perform(click());
    }

    @Test
    public void testLoginAndLogout() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        try {
            onView(withText(R.string.action_login)).check(matches(isDisplayed()));
            login();
        } catch (NoMatchingViewException e) {
            logout();
        }
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        try {
            onView(withText(R.string.action_logout)).check(matches(isDisplayed()));
            logout();
        } catch (NoMatchingViewException e) {
            login();
        }
    }

    private void login() {
        onView(withText(R.string.action_login)).perform(click());
        onView(withId(R.id.username)).perform(click(), typeText("flavius_p"));
        onView(withId(R.id.password)).perform(click(), typeText("flaviusp"));
        onView(withId(R.id.sign_in_btn)).perform(click());
    }

    private void logout() {
        onView(withText(R.string.action_logout)).perform(click());
    }
}

