package com.example.fpapara.themoviedb.ui.movielist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.login.LoginActivity;
import com.example.fpapara.themoviedb.util.SharedPref;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.viewpager)
    ViewPager mViewPager;
    @BindView(R.id.tabs)
    TabLayout mTabLayout;
    private final String FRAGMENT_TYPE = "TYPE";
    private Menu mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        setupViewPager(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mMenu != null) {
            updateMenuItem();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.mMenu = menu;
        updateMenuItem();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            if (checkIfLogged()) {
                //there is no api for logout
                //we clear the session id from shared preferences
                SharedPref.writeData(this, TmdbConstants.SharedPrefs.SESSION_ID, null);
                SharedPref.writeData(this, TmdbConstants.SharedPrefs.LOGGED_USERNAME, null);
                updateMenuItem();
            } else {
                startActivity(new Intent(this, LoginActivity.class));
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void updateMenuItem() {
        MenuItem menuItem = mMenu.findItem(R.id.action_settings);
        MenuItem menuItemUsername = mMenu.findItem(R.id.logged_user);
        if (checkIfLogged()) {
            menuItem.setTitle(R.string.action_logout);
            menuItemUsername.setVisible(true);
            String logged_user_text = "Logged as: " + SharedPref.getData(this, TmdbConstants.SharedPrefs.LOGGED_USERNAME, "");
            menuItemUsername.setTitle(logged_user_text);
        } else {
            menuItem.setTitle(R.string.action_login);
            menuItemUsername.setTitle("");
            menuItemUsername.setVisible(false);
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        MovieListViewPagerAdapter adapter = new MovieListViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(newFragment(1), getString(R.string.movie_popular));
        adapter.addFragment(newFragment(2), getString(R.string.movie_upcoming));
        viewPager.setAdapter(adapter);
    }

    private Fragment newFragment(int fragmentType) {
        Fragment myFragment = new MovieListFragment();
        Bundle args = new Bundle();
        args.putInt(FRAGMENT_TYPE, fragmentType);
        myFragment.setArguments(args);
        return myFragment;
    }

    private boolean checkIfLogged() {
        String sessionID = SharedPref.getData(this, TmdbConstants.SharedPrefs.SESSION_ID, null);
        if (sessionID != null) {
            return true;
        } else {
            return false;
        }
    }
}
