package com.example.fpapara.themoviedb.ui.movielist.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.moviedetails.MovieDetailsActivity;
import com.example.fpapara.themoviedb.ui.movielist.model.Genre;
import com.example.fpapara.themoviedb.ui.movielist.model.Result;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieRecyclerViewAdapter extends RecyclerView.Adapter<MovieRecyclerViewAdapter.MovieViewHolder> {
    private final List<Result> mDataset;
    private final NumberFormat mFormatter;
    private static final String MOVIE_ID = "MOVIE_ID";

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.movieName)
        TextView movieName;
        @BindView(R.id.movieType)
        TextView movieType;
        @BindView(R.id.movieYear)
        TextView movieYear;
        @BindView(R.id.movieRating)
        TextView movieRating;
        @BindView(R.id.movieDescription)
        TextView movieDescription;
        @BindView(R.id.movieImage)
        ImageView moviePoster;

        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            goToMovieDetails(itemView.getContext(), getAdapterPosition());
        }

        private void goToMovieDetails(Context context, Integer itemPosition) {
            String movieId = mDataset.get(itemPosition).getId().toString();
            Intent intent = new Intent(context, MovieDetailsActivity.class);
            intent.putExtra(MOVIE_ID, movieId);
            context.startActivity(intent);
        }
    }

    public MovieRecyclerViewAdapter(List<Result> myDataset) {
        mDataset = myDataset;
        mFormatter = NumberFormat.getInstance(Locale.UK);
        mFormatter.setMinimumFractionDigits(0);
        mFormatter.setMaximumFractionDigits(2);
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {

        holder.movieName.setText(mDataset.get(position).getTitle());
        holder.movieType.setText(getGenresName(mDataset.get(position).getGenreIds()));
        holder.movieDescription.setText(mDataset.get(position).getOverview());
        holder.movieRating.setText(mFormatter.format(mDataset.get(position).getVoteAverage()));
        holder.movieYear.setText(mDataset.get(position).getReleaseDate().substring(0, 4));
        String url = TmdbConstants.Api.DEFAULT_IMAGES_URL + mDataset.get(position).getPosterPath();

        Glide.with(holder.moviePoster.getContext())
                .load(url)
                .placeholder(R.mipmap.no_poster_movie)
                .into(holder.moviePoster);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private String getGenresName(List<Integer> genreIds) {
        StringBuilder genresString = new StringBuilder();
        Genre genre = new Genre();
        for (int i = 0; i < genreIds.size(); i++) {
            if (i != 0) {
                genresString.append(", ");
                genresString.append(genre.getGenreName(genreIds.get(i)));
            } else {
                genresString.append(genre.getGenreName(genreIds.get(i)));
            }
        }
        return genresString.toString();
    }
}
