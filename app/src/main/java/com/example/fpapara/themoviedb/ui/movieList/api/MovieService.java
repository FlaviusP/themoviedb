package com.example.fpapara.themoviedb.ui.movielist.api;

import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.movielist.model.MovieList;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieService {
    private MovieList mMovieList;
    private int mMovieServiceType;
    private static final int MOVIE_SERVICE_POPULAR = 1;
    private static final int MOVIE_SERVICE_UPCOMING = 2;
    private static final String ARRANGE_BY_VOTE_DESCENDANT = "vote_average.desc";
    private int mPageNumber;

    public void setType(int movieServiceType) {
        mMovieServiceType = movieServiceType;
        mPageNumber = 1;
    }

    public void loadMovies(Integer pageNumber) {
        mPageNumber = pageNumber;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TmdbConstants.Api.DEFAULT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        switch (mMovieServiceType) {
            case MOVIE_SERVICE_POPULAR:
                String pathPopular = TmdbConstants.Api.MOVIE_PATH + TmdbConstants.Api.POPULAR_MOVIE_PATH;
                loadFromApi(retrofit, pathPopular, ARRANGE_BY_VOTE_DESCENDANT,
                        TmdbConstants.Extras.LOAD_SUCCESSFULLY_POPULAR);
                break;
            case MOVIE_SERVICE_UPCOMING:
                String pathUpcoming = TmdbConstants.Api.MOVIE_PATH + TmdbConstants.Api.UPCOMING_MOVIE_PATH;
                loadFromApi(retrofit, pathUpcoming, ARRANGE_BY_VOTE_DESCENDANT,
                        TmdbConstants.Extras.LOAD_SUCCESSFULLY_UPCOMING);
                break;
        }
    }

    private void loadFromApi(Retrofit retrofit, String moviePath, String sort, final String successString) {
        MoviesApi service = retrofit.create(MoviesApi.class);
        Call<MovieList> call = service.movieList(moviePath, TmdbConstants.Api.KEY, mPageNumber, sort);
        call.enqueue(new Callback<MovieList>() {
            @Override
            public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                mMovieList = response.body();
                postEvent(successString);
            }

            @Override
            public void onFailure(Call<MovieList> call, Throwable t) {
                postErrorEvent();
            }
        });
    }

    private MovieList getMovieList() {
        return mMovieList;
    }

    @Subscribe
    private void postEvent(String successString) {
        EventBus.getDefault().postSticky(getMovieList());
        EventBus.getDefault().post(new MessageEvent(successString));
    }

    @Subscribe
    private void postErrorEvent() {
        EventBus.getDefault().post(new MessageEvent(TmdbConstants.Extras.LOAD_FAILED));
    }
}
