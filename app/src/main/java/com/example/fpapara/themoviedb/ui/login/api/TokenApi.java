package com.example.fpapara.themoviedb.ui.login.api;

import com.example.fpapara.themoviedb.ui.login.model.Token;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TokenApi {
    @GET("{path}")
    Call<Token> getToken(
            @Path(value = "path", encoded = true) String path,
            @Query(value = "api_key") String apiKey);
}
