package com.example.fpapara.themoviedb.ui.moviedetails;

import android.content.Context;
import android.widget.ImageView;

public interface MovieDetailsContract {

    void setTitle(String title);

    void setOverview(String overview);

    void setVote(String vote);

    void setReleaseDate(String releaseDate);

    void setTagLine(String tagLine);

    void setHomePage(String homePage);

    void setStatus(String status);

    void setGenre(String genre);

    Context getContext();

    ImageView getImageView();

    void hideAllItemShowLoading(Boolean hide);
}
