package com.example.fpapara.themoviedb.ui.login;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.ui.login.api.LoginService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginActivityContract {

    @BindView(R.id.username)
    EditText mUsernameView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.login_progress)
    View mProgressView;
    @BindView(R.id.sign_in_btn)
    Button mSignInButton;
    private LoginActivityPresenter mLoginActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mLoginActivityPresenter = new LoginActivityPresenter(this, new LoginService());
    }

    @Override
    protected void onDestroy() {
        mLoginActivityPresenter.onDestroy();
        super.onDestroy();
    }

    @OnClick(R.id.sign_in_btn)
    public void submit() {
        mLoginActivityPresenter.login();
    }

    @Override
    public String getUsername() {
        return mUsernameView.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPasswordView.getText().toString();
    }

    @Override
    public void setErrorNull() {
        mUsernameView.setError(null);
        mPasswordView.setError(null);
    }

    @Override
    public void setErrorPasswordToShort(String message) {
        mPasswordView.setError(message);
    }

    @Override
    public void setErrorUsernameInvalid(String message) {
        mUsernameView.setError(message);
    }

    @Override
    public void setFocusOnView(View view) {
        view.requestFocus();
    }

    @Override
    public View getPasswordView() {
        return mPasswordView;
    }

    @Override
    public View getUsernameView() {
        return mUsernameView;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(final boolean show) {
        if (show) {
            mProgressView.setVisibility(View.VISIBLE);
        } else {
            mProgressView.setVisibility(View.GONE);
        }
    }

    @Override
    public void inputOnOff(boolean on) {
        mUsernameView.setEnabled(on);
        mPasswordView.setEnabled(on);
        mSignInButton.setEnabled(on);
    }

    @Override
    public void finishLogin() {
        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }
}

