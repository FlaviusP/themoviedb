package com.example.fpapara.themoviedb.ui.movielist.api;

import com.example.fpapara.themoviedb.ui.movielist.model.MovieList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MoviesApi {
    @GET("{path}")
    Call<MovieList> movieList(
            @Path(value = "path", encoded = true) String path,
            @Query(value = "api_key") String apiKey,
            @Query(value = "page") Integer page,
            @Query(value = "sort_by") String sort);
}
