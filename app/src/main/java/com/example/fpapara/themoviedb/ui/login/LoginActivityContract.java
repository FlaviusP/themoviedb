package com.example.fpapara.themoviedb.ui.login;

import android.content.Context;
import android.view.View;

/**
 * Created by fpapara on 5/13/2016.
 */
public interface LoginActivityContract {

    String getUsername();

    String getPassword();

    void showProgress(boolean show);

    void setErrorNull();

    void setErrorPasswordToShort(String message);

    void setErrorUsernameInvalid(String message);

    void setFocusOnView(View view);

    View getPasswordView();

    View getUsernameView();

    void showToast(String message);

    void inputOnOff(boolean on);

    void finishLogin();

    Context getContext();
}
