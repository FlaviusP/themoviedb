package com.example.fpapara.themoviedb.ui.moviedetails;

import com.bumptech.glide.Glide;
import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.moviedetails.api.MovieDetailsService;
import com.example.fpapara.themoviedb.ui.moviedetails.model.Genre;
import com.example.fpapara.themoviedb.ui.moviedetails.model.MovieDetails;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class MovieDetailsPresenter {

    private final MovieDetailsContract mView;

    public MovieDetailsPresenter(MovieDetailsContract view, MovieDetailsService movieService) {
        this.mView = view;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mView.hideAllItemShowLoading(true);
        movieService.loadMovieDetails();
    }

    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        if (event.message.equals(TmdbConstants.Extras.LOAD_SUCCESSFULLY_DETAILS)) {
            MovieDetails movieDetails = EventBus.getDefault().getStickyEvent(MovieDetails.class);
            mView.setTitle(movieDetails.getTitle());
            mView.setTagLine("(" + movieDetails.getTagline() + ")");

            String url = TmdbConstants.Api.DEFAULT_IMAGES_URL + movieDetails.getPosterPath();
            Glide.with(mView.getContext())
                    .load(url)
                    .asBitmap()
                    .placeholder(R.mipmap.no_poster_movie)
                    .into(mView.getImageView());
            mView.hideAllItemShowLoading(false);

            mView.setVote(movieDetails.getVoteAverage().toString());
            mView.setReleaseDate(movieDetails.getReleaseDate());

            String statusString = mView.getContext().getString(R.string.status)
                    + ' ' + movieDetails.getStatus();
            mView.setStatus(statusString);

            String genreString = mView.getContext().getString(R.string.genre)
                    + ' ' + getAllGenreToString(movieDetails.getGenres());

            mView.setGenre(genreString);
            mView.getContext().getString(R.string.genre);
            mView.setOverview(movieDetails.getOverview());
            mView.setHomePage(movieDetails.getHomepage());
        }
    }

    private String getAllGenreToString(List<Genre> genresList) {
        StringBuilder genres = new StringBuilder();
        Integer numberOfGenre = genresList.size();
        for (int i = 0; i < numberOfGenre; i++) {
            genres.append(genresList.get(i).getName());
            if (i < (numberOfGenre - 1)) {
                genres.append(", ");
            }
        }
        return genres.toString();
    }
}
