package com.example.fpapara.themoviedb;

/**
 * Created by fpapara on 4/26/2016.
 */
public class TmdbConstants {
    public static final class Extras {
        public static final String LOAD_SUCCESSFULLY_POPULAR = "LOAD_SUCCESSFULLY_POPULAR";
        public static final String LOAD_SUCCESSFULLY_UPCOMING = "LOAD_SUCCESSFULLY_UPCOMING";
        public static final String LOAD_SUCCESSFULLY_DETAILS = "LOAD_SUCCESSFULLY_DETAILS";
        public static final String LOAD_FAILED = "LOAD_FAILED";
        public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";

        public static final String LOGIN_FAILED_CAN_NOT_GET_TOKEN = "LOGIN_FAILED_CAN_NOT_GET_TOKEN";
        public static final String LOGIN_FAILED_BAD_USERNAME_OR_PASSWORD = "LOGIN_FAILED_BAD_USERNAME_OR_PASSWORD";
        public static final String LOGIN_FAILED_CAN_NOT_GET_SESSION_ID = "LOGIN_FAILED_CAN_NOT_GET_SESSION_ID";
        public static final String LOGIN_CHECK_INTERNET_CONNECTION = "LOGIN_CHECK_INTERNET_CONNECTION";
        public static final String LOGIN_FAILED_BAD_REQUEST = "Bad request";
        public static final String LOGIN_FAILED_UNAUTHORIZED = "Unauthorized";
    }

    public static final class Api {
        public static final String KEY = "e0bec78e9650834b23a33436bca657f9";
        public static final String DEFAULT_URL = "https://api.themoviedb.org/3/";
        public static final String DEFAULT_IMAGES_URL = "http://image.tmdb.org/t/p/w300/";
        public static final String MOVIE_PATH = "movie";
        public static final String POPULAR_MOVIE_PATH = "/popular";
        public static final String UPCOMING_MOVIE_PATH = "/upcoming";
        public static final String DEFAULT_LOGIN_PATH = "authentication/token";
        public static final String DEFAULT_LOGIN_REQUEST_TOKEN = "/new";
        public static final String DEFAULT_LOGIN_VALIDATE = "/validate_with_login";
        public static final String DEFAULT_LOGIN_REQUEST_SESSION_ID = "authentication/session/new";


    }

    public static final class SharedPrefs {
        public static final String TOKEN = "TOKEN";
        public static final String TOKEN_EXPIRATION = "TOKEN_EXPIRATION";
        public static final String SESSION_ID = "SESSION_ID";
        public static final int TOKEN_EXPIRATION_IN_MINUTES = 60;
        public static final String LOGGED_USERNAME = "LOGGED_USERNAME";
    }
}
