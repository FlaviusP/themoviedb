package com.example.fpapara.themoviedb.ui.login.api;

import android.content.Context;

import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.login.model.Token;
import com.example.fpapara.themoviedb.util.SharedPref;
import com.example.fpapara.themoviedb.util.UtcDateTime;

import java.util.Date;

public class TokenOperation {

    public static void save(Token token, Context context) {
        SharedPref.writeData(context, TmdbConstants.SharedPrefs.TOKEN, token.getRequestToken());
        SharedPref.writeData(context, TmdbConstants.SharedPrefs.TOKEN_EXPIRATION, token.getExpiresAt());
    }

    public static void delete(Context context) {
        SharedPref.writeData(context, TmdbConstants.SharedPrefs.TOKEN, null);
        SharedPref.writeData(context, TmdbConstants.SharedPrefs.TOKEN_EXPIRATION, null);
    }

    public static String get(Context context) {
        return SharedPref.getData(context, TmdbConstants.SharedPrefs.TOKEN, null);
    }

    public static boolean have(Context context) {
        return SharedPref.getData(context, TmdbConstants.SharedPrefs.TOKEN, null) != null;
    }

    public static boolean isValid(Context context) {
        String token = SharedPref.getData(context, TmdbConstants.SharedPrefs.TOKEN, null);
        String tokenExpiration = SharedPref.getData(context, TmdbConstants.SharedPrefs.TOKEN_EXPIRATION, null);
        return token != null && checkTokenExpirationTime(TmdbConstants.SharedPrefs.TOKEN_EXPIRATION_IN_MINUTES, tokenExpiration);
    }

    private static boolean checkTokenExpirationTime(int maxDifferenceInMinutes, String initialDate) {
        Date tokenData = UtcDateTime.StringDateToDate(initialDate);
        Date now = new Date();
        long diff = tokenData.getTime() - now.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        return (minutes <= maxDifferenceInMinutes) && (minutes > 0);
    }
}
