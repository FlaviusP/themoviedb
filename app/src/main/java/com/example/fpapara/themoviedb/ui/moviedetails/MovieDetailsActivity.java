package com.example.fpapara.themoviedb.ui.moviedetails;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.ui.moviedetails.api.MovieDetailsService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailsActivity extends AppCompatActivity implements MovieDetailsContract {

    @BindView(R.id.movieTitle)
    TextView mMovieTitle;
    @BindView(R.id.moviePoster)
    ImageView mMoviePoster;
    @BindView(R.id.overview)
    TextView mMovieDescription;
    @BindView(R.id.note)
    TextView mVoteAverage;
    @BindView(R.id.releaseDate)
    TextView mReleaseDate;
    @BindView(R.id.status)
    TextView mStatus;
    @BindView(R.id.homePage)
    TextView mHomePage;
    @BindView(R.id.tagLine)
    TextView mTagLine;
    @BindView(R.id.genre)
    TextView mGenre;
    @BindView(R.id.allItems)
    LinearLayout allItemsView;
    @BindView(R.id.movieDetailsProgressBar)
    ProgressBar mProgressBar;

    private MovieDetailsPresenter mPresenter;
    private static final String MOVIE_ID = "MOVIE_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String id = extras.getString(MOVIE_ID);
            mPresenter = new MovieDetailsPresenter(this, new MovieDetailsService(id));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void setTitle(String title) {
        mMovieTitle.setText(title);
    }

    @Override
    public void setOverview(String overview) {
        mMovieDescription.setText(overview);
    }

    @Override
    public void setVote(String vote) {
        mVoteAverage.setText(vote);
    }

    @Override
    public void setReleaseDate(String releaseDate) {
        mReleaseDate.setText(releaseDate);
    }

    @Override
    public void setTagLine(String tagLine) {
        mTagLine.setText(tagLine);
    }

    @Override
    public void setHomePage(String homePage) {
        mHomePage.setText(homePage);
    }

    @Override
    public void setStatus(String status) {
        mStatus.setText(status);
    }

    @Override
    public void setGenre(String genre) {
        mGenre.setText(genre);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public ImageView getImageView() {
        return mMoviePoster;
    }

    @Override
    public void hideAllItemShowLoading(Boolean hide) {
        if (hide) {
            allItemsView.setVisibility(LinearLayout.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            allItemsView.setVisibility(LinearLayout.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }
}
