package com.example.fpapara.themoviedb.ui.login.api;

import com.example.fpapara.themoviedb.ui.login.model.SessionId;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SessionIdApi {
    @GET("{path}")
    Call<SessionId> getSessionId(
            @Path(value = "path", encoded = true) String path,
            @Query(value = "api_key") String apiKey,
            @Query(value = "request_token") String token);
}
