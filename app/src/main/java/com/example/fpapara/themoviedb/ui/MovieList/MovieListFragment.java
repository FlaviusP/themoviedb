package com.example.fpapara.themoviedb.ui.movielist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.ui.movielist.api.MovieService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListFragment extends Fragment implements MovieListFragmentContract {

    @BindView(R.id.movieRecyclerView)
    RecyclerView mMovieRecyclerView;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    private MovieListFragmentPresenter mPresenter;
    private int mFragmentType;
    private LinearLayoutManager mLayoutManager;
    private Integer mPageNumber;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private final int mVisibleThreshold = 5;
    private int mFirstVisibleItem;
    private int mVisibleItemCount;
    private int mTotalItemCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);
        if (getArguments() != null) {
            String FRAGMENT_TYPE = "TYPE";
            this.setType(this.getArguments().getInt(FRAGMENT_TYPE, 1));
        }
        ButterKnife.bind(this, view);
        // TODO: 5/10/2016  - used for Espresso testing
        view.setId(R.id.movieRecyclerView + mFragmentType * 1000);
        mMovieRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                mVisibleItemCount = mMovieRecyclerView.getChildCount();
                mTotalItemCount = mLayoutManager.getItemCount();
                mFirstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if (mLoading) {
                    if (mTotalItemCount > mPreviousTotal) {
                        mLoading = false;
                        mPreviousTotal = mTotalItemCount;
                    }
                }
                if (!mLoading && (mTotalItemCount - mVisibleItemCount)
                        <= (mFirstVisibleItem + mVisibleThreshold)) {
                    mPageNumber++;
                    getMovieList(mPageNumber);
                    mLoading = true;
                }
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        MovieService movieService = new MovieService();
        mPageNumber = 1;
        movieService.setType(mFragmentType);
        mPresenter = new MovieListFragmentPresenter(this, movieService);
        getMovieList(mPageNumber);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void initRecyclerView(RecyclerView.Adapter adapter) {
        mLayoutManager = new LinearLayoutManager(getContext());
        if (mMovieRecyclerView != null) {
            mMovieRecyclerView.setLayoutManager(mLayoutManager);
            mMovieRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mMovieRecyclerView.setAdapter(adapter);
        }
    }

    @Override
    public int getType() {
        return mFragmentType;
    }

    @Override
    public void setType(int type) {
        mFragmentType = type;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getMovieList(Integer page) {
        mPresenter.loadMoviesFromApi(page);
    }

    @Override
    public void loadMoviesFailed(int resID) {
        Toast.makeText(getContext(), getString(resID), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startShowLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopShowLoading() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }
}