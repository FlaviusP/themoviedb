package com.example.fpapara.themoviedb.ui.login;

import android.content.Context;
import android.text.TextUtils;

import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.login.api.LoginService;
import com.example.fpapara.themoviedb.util.SharedPref;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class LoginActivityPresenter {
    private final LoginActivityContract mView;
    private final LoginService mLoginService;
    private String mUsername;
    private Context mContext;

    public LoginActivityPresenter(LoginActivityContract view, LoginService loginService) {
        this.mView = view;
        this.mLoginService = loginService;
        this.mContext = view.getContext();
        EventBus.getDefault().register(this);
    }

    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    public void login() {
        mUsername = mView.getUsername();
        String password = mView.getPassword();
        mView.inputOnOff(false);
        mView.setErrorNull();
        boolean cancel = false;

        if (!TextUtils.isEmpty(mUsername) &&
                !isPasswordValid(password)) {
            mView.setErrorPasswordToShort(mContext.getString(R.string.error_invalid_password));
            mView.setFocusOnView(mView.getPasswordView());
            cancel = true;
        }

        if (TextUtils.isEmpty(mUsername)) {
            mView.setErrorUsernameInvalid(mContext.getString(R.string.error_field_required));
            mView.setFocusOnView(mView.getUsernameView());
            cancel = true;
        } else if (!isUsernameValid(mUsername)) {
            mView.setErrorUsernameInvalid(mContext.getString(R.string.error_invalid_email));
            mView.setFocusOnView(mView.getUsernameView());
            cancel = true;
        }

        if (!cancel) {
            mView.showProgress(true);
            mLoginService.login(mContext, mUsername, password);
        }
    }

    private boolean isUsernameValid(String username) {
        return username.length() > 3;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 3;
    }

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        mView.inputOnOff(true);
        switch (event.message) {
            default:
            case TmdbConstants.Extras.LOGIN_SUCCESS:
                mView.showProgress(false);
                mView.finishLogin();
                SharedPref.writeData(mContext, TmdbConstants.SharedPrefs.LOGGED_USERNAME, mUsername);
                break;
            case TmdbConstants.Extras.LOGIN_FAILED_CAN_NOT_GET_TOKEN:
                restartLoginActivity(mContext.getString(R.string.login_error_token));
                break;
            case TmdbConstants.Extras.LOGIN_FAILED_BAD_USERNAME_OR_PASSWORD:
                restartLoginActivity(mContext.getString(R.string.login_error_user_password));
                break;
            case TmdbConstants.Extras.LOGIN_FAILED_CAN_NOT_GET_SESSION_ID:
                restartLoginActivity(mContext.getString(R.string.login_error_session_id));
                break;
            case TmdbConstants.Extras.LOGIN_CHECK_INTERNET_CONNECTION:
                restartLoginActivity(mContext.getString(R.string.login_error_check_internet_connection));
                break;
        }
    }

    private void restartLoginActivity(String message) {
        mView.showProgress(false);
        mView.setErrorNull();
        mView.showToast(message);
    }
}
