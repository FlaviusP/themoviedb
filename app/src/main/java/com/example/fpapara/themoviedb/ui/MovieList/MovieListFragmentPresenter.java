package com.example.fpapara.themoviedb.ui.movielist;

import android.support.v7.widget.RecyclerView;

import com.example.fpapara.themoviedb.R;
import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.movielist.adapter.MovieRecyclerViewAdapter;
import com.example.fpapara.themoviedb.ui.movielist.api.MovieService;
import com.example.fpapara.themoviedb.ui.movielist.model.MovieList;
import com.example.fpapara.themoviedb.ui.movielist.model.Result;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;


public class MovieListFragmentPresenter {
    private final MovieService mMovieService;
    private final MovieListFragmentContract mView;
    private Integer mPagePopular;
    private Integer mPageUpcoming;
    private List<Result> mMovieList;
    private RecyclerView.Adapter mAdapter;
    private static final Integer FRAGMENT_POPULAR = 1;
    private static final Integer FRAGMENT_UPCOMING = 2;


    public MovieListFragmentPresenter(MovieListFragmentContract view, MovieService movieService) {
        this.mView = view;
        this.mMovieService = movieService;
        mPagePopular = 1;
        mPageUpcoming = 1;
    }

    @Subscribe
    public void loadMoviesFromApi(Integer page) {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Integer viewType = mView.getType();
        if (viewType.equals(FRAGMENT_POPULAR)) {
            mPagePopular = page;
            mMovieService.loadMovies(mPagePopular);
        }
        if (viewType.equals(FRAGMENT_UPCOMING)) {
            mPageUpcoming = page;
            mMovieService.loadMovies(mPageUpcoming);
        }
        mView.startShowLoading();
    }

    @Subscribe
    public void onMessageEvent(MessageEvent event) {
        if (isNewMovieEvent(event)) {
            loadNewMovies(event);
        } else {
            if (event.message.equals(TmdbConstants.Extras.LOAD_FAILED)) {
                loadFailedMessage();
            }
        }
        mView.stopShowLoading();
    }

    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    private List<Result> getMovieListItems() {
        MovieList stickyEvent = EventBus.getDefault().removeStickyEvent(MovieList.class);
        if (stickyEvent != null && (stickyEvent.getResults() != null)) {
            return stickyEvent.getResults();
        } else {
            return new ArrayList<>(0);
        }
    }

    private boolean isNewMovieEvent(MessageEvent event) {
        if ((event.message.equals(TmdbConstants.Extras.LOAD_SUCCESSFULLY_POPULAR))
                || (event.message.equals(TmdbConstants.Extras.LOAD_SUCCESSFULLY_UPCOMING))) {
            return true;
        }
        if (event.message.equals(TmdbConstants.Extras.LOAD_FAILED)) {
            return false;
        }
        return false;
    }

    private void loadNewMovies(MessageEvent event) {
        int viewType = mView.getType();
        if (event.message.equals(TmdbConstants.Extras.LOAD_SUCCESSFULLY_POPULAR) && (viewType == FRAGMENT_POPULAR)) {
            if (isFirstPageNumber(mPagePopular)) {
                initRecyclerView();
            } else {
                addToRecyclerView();
            }
        }
        if (event.message.equals(TmdbConstants.Extras.LOAD_SUCCESSFULLY_UPCOMING) && (viewType == FRAGMENT_UPCOMING)) {
            if (isFirstPageNumber(mPageUpcoming)) {
                initRecyclerView();
            } else {
                addToRecyclerView();
            }
        }
    }

    private boolean isFirstPageNumber(Integer page) {
        return page.equals(1);
    }

    private void initRecyclerView() {
        mMovieList = getMovieListItems();
        mAdapter = new MovieRecyclerViewAdapter(mMovieList);
        mView.initRecyclerView(mAdapter);
    }

    private void addToRecyclerView() {
        mMovieList.addAll(getMovieListItems());
        mAdapter.notifyDataSetChanged();
    }

    private void loadFailedMessage() {
        mView.loadMoviesFailed(R.string.err_failed_to_load);
    }
}
