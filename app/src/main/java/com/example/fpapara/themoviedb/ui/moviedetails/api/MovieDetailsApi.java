package com.example.fpapara.themoviedb.ui.moviedetails.api;

import com.example.fpapara.themoviedb.ui.moviedetails.model.MovieDetails;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieDetailsApi {
    @GET("{path}")
    Call<MovieDetails> loadDetails(
            @Path(value = "path", encoded = true) String path,
            @Query(value = "api_key") String apiKey);
}
