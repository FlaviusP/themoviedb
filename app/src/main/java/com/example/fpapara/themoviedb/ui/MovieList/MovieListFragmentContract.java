package com.example.fpapara.themoviedb.ui.movielist;

import android.support.v7.widget.RecyclerView;

public interface MovieListFragmentContract {

    void getMovieList(Integer page);

    void loadMoviesFailed(int resID);

    void startShowLoading();

    void stopShowLoading();

    void initRecyclerView(RecyclerView.Adapter adapter);

    int getType();

    void setType(int type);
}
