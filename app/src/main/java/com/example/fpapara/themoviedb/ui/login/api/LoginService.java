package com.example.fpapara.themoviedb.ui.login.api;

import android.content.Context;

import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.login.model.LoginSuccess;
import com.example.fpapara.themoviedb.ui.login.model.SessionId;
import com.example.fpapara.themoviedb.ui.login.model.Token;
import com.example.fpapara.themoviedb.util.SharedPref;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginService {

    private Token mToken;
    private LoginSuccess mLoginSuccess;
    private String mUsername;
    private String mPassword;
    private Context mContext;
    private Retrofit mRetrofit;

    @Subscribe
    public void login(Context context, String username, String password) {
        this.mContext = context;
        this.mUsername = username;
        this.mPassword = password;
        initRetrofit();
        if (TokenOperation.have(mContext) && TokenOperation.isValid(mContext)) {
            requestLogin(mRetrofit, getLoginPath(), TokenOperation.get(mContext));
        } else {
            requestNewToken(mRetrofit, getTokenPath());
        }
    }

    private void initRetrofit() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(TmdbConstants.Api.DEFAULT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private void requestNewToken(Retrofit retrofit, String path) {
        TokenApi service = retrofit.create(TokenApi.class);
        Call<Token> call = service.getToken(path, TmdbConstants.Api.KEY);
        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (checkResponseMessage(response.message())) {
                    mToken = response.body();
                    TokenOperation.save(mToken, mContext);
                    requestLogin(mRetrofit, getLoginPath(), mToken.getRequestToken());
                } else {
                    sendLoginFailedEvent(TmdbConstants.Extras.LOGIN_FAILED_CAN_NOT_GET_TOKEN);
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                sendLoginFailedEvent(TmdbConstants.Extras.LOGIN_CHECK_INTERNET_CONNECTION);
            }
        });
    }

    private void requestLogin(Retrofit retrofit, String path, final String token) {
        LoginApi service = retrofit.create(LoginApi.class);
        Call<LoginSuccess> call = service.loginToMovieDb(path, TmdbConstants.Api.KEY, token, mUsername, mPassword);
        call.enqueue(new Callback<LoginSuccess>() {
            @Override
            public void onResponse(Call<LoginSuccess> call, Response<LoginSuccess> response) {
                if (checkResponseMessage(response.message())) {
                    mLoginSuccess = response.body();
                    requestNewSessionId(mRetrofit, getSessionPath(), token);
                } else {
                    sendLoginFailedEvent(TmdbConstants.Extras.LOGIN_FAILED_BAD_USERNAME_OR_PASSWORD);
                }
            }

            @Override
            public void onFailure(Call<LoginSuccess> call, Throwable t) {
                sendLoginFailedEvent(TmdbConstants.Extras.LOGIN_CHECK_INTERNET_CONNECTION);
            }
        });
    }

    private void requestNewSessionId(Retrofit retrofit, String path, final String token) {
        final SessionIdApi service = retrofit.create(SessionIdApi.class);
        Call<SessionId> call = service.getSessionId(path, TmdbConstants.Api.KEY, token);
        call.enqueue(new Callback<SessionId>() {
            @Override
            public void onResponse(Call<SessionId> call, Response<SessionId> response) {
                if (checkResponseMessage(response.message())) {
                    SessionId session = response.body();
                    saveSessionId(session);
                    TokenOperation.delete(mContext);
                    sendLoginSuccessEvent();
                } else {
                    sendLoginFailedEvent(TmdbConstants.Extras.LOGIN_FAILED_CAN_NOT_GET_SESSION_ID);
                }
            }

            @Override
            public void onFailure(Call<SessionId> call, Throwable t) {
                sendLoginFailedEvent(TmdbConstants.Extras.LOGIN_CHECK_INTERNET_CONNECTION);
            }
        });
    }

    private void saveSessionId(SessionId sessionId) {
        SharedPref.writeData(mContext, TmdbConstants.SharedPrefs.SESSION_ID, sessionId.getSessionId());
    }

    private String getTokenPath() {
        return TmdbConstants.Api.DEFAULT_LOGIN_PATH + TmdbConstants.Api.DEFAULT_LOGIN_REQUEST_TOKEN;
    }

    private String getLoginPath() {
        return TmdbConstants.Api.DEFAULT_LOGIN_PATH + TmdbConstants.Api.DEFAULT_LOGIN_VALIDATE;
    }

    private String getSessionPath() {
        return TmdbConstants.Api.DEFAULT_LOGIN_REQUEST_SESSION_ID;
    }

    private Boolean checkResponseMessage(String message) {
        return !(message.equals(TmdbConstants.Extras.LOGIN_FAILED_BAD_REQUEST) ||
                message.equals(TmdbConstants.Extras.LOGIN_FAILED_UNAUTHORIZED));
    }

    @Subscribe
    private void sendLoginSuccessEvent() {
        EventBus.getDefault().post(new MessageEvent(TmdbConstants.Extras.LOGIN_SUCCESS));
    }

    @Subscribe
    private void sendLoginFailedEvent(String message) {
        EventBus.getDefault().post(new MessageEvent(message));
    }
}
