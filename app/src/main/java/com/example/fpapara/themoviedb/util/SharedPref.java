package com.example.fpapara.themoviedb.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by fpapara on 5/12/2016.
 */
public class SharedPref {

    public static String getData(Context con, String variable, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
        return prefs.getString(variable, defaultValue);
    }

    public static void writeData(Context con, String variable, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(con);
        prefs.edit().putString(variable, defaultValue).apply();
    }
}
