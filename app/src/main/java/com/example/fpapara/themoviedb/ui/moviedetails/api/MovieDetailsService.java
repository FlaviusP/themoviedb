package com.example.fpapara.themoviedb.ui.moviedetails.api;

import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.moviedetails.model.MovieDetails;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MovieDetailsService {

    private MovieDetails mMovieDetails;
    private final String mId;

    public MovieDetailsService(String id) {
        this.mId = id;
    }

    public void loadMovieDetails() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(TmdbConstants.Api.DEFAULT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        String path = TmdbConstants.Api.MOVIE_PATH + "/" + mId;
        loadFromApi(retrofit, path, TmdbConstants.Extras.LOAD_SUCCESSFULLY_DETAILS);
    }

    private void loadFromApi(Retrofit retrofit, String moviePath, final String successString) {
        MovieDetailsApi service = retrofit.create(MovieDetailsApi.class);
        Call<MovieDetails> call = service.loadDetails(moviePath, TmdbConstants.Api.KEY);
        call.enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                mMovieDetails = response.body();
                postEvent(successString);
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {
                postErrorEvent();
            }
        });
    }

    @Subscribe
    private void postEvent(String successString) {
        EventBus.getDefault().postSticky(mMovieDetails);
        EventBus.getDefault().post(new MessageEvent(successString));
    }

    @Subscribe
    private void postErrorEvent() {
        EventBus.getDefault().post(new MessageEvent(TmdbConstants.Extras.LOAD_FAILED));
    }
}
