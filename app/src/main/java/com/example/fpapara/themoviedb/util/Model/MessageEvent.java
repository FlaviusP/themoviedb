package com.example.fpapara.themoviedb.util.model;

public class MessageEvent {
    public final String message;

    public MessageEvent(String message) {
        this.message = message;
    }
}

