package com.example.fpapara.themoviedb.ui.movielist;

import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.movielist.adapter.MovieRecyclerViewAdapter;
import com.example.fpapara.themoviedb.ui.movielist.api.MovieService;
import com.example.fpapara.themoviedb.ui.movielist.model.MovieList;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.when;

public class MovieListFragmentPresenterTest {

    MovieListFragmentPresenter mPresenter;
    @Mock
    MovieService mMovieService;
    @Mock
    MovieListFragmentContract mMovieListContract;
    EventBus mEventBus;
    Integer page;

    @Before
    public void setUp() throws Exception {
        mMovieService = Mockito.mock(MovieService.class);
        mMovieListContract = Mockito.mock(MovieListFragmentContract.class);

        when(mMovieListContract.getType()).thenReturn(1);
        int expected = 1;
        assertEquals(expected, mMovieListContract.getType());

        mEventBus = EventBus.getDefault();
        page = 1;
        mPresenter = new MovieListFragmentPresenter(mMovieListContract, mMovieService);
        mPresenter.loadMoviesFromApi(page);
    }

    @Test
    public void testLoadMovies() throws Exception {
        Mockito.verify(mMovieService).loadMovies(page);
    }

    @Test
    public void testLoadMoviesInRecyclerView() {
        MovieList mList = new MovieList();
        mEventBus.postSticky(mList);
        mEventBus.post(new MessageEvent(TmdbConstants.Extras.LOAD_SUCCESSFULLY_POPULAR));

        Mockito.verify(mMovieListContract).startShowLoading();
        Mockito.verify(mMovieListContract).initRecyclerView(new MovieRecyclerViewAdapter(anyList()));
        Mockito.verify(mMovieListContract, Mockito.atLeast(1)).stopShowLoading();
    }
}