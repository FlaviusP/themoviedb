package com.example.fpapara.themoviedb.ui.moviedetails;

import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.moviedetails.api.MovieDetailsService;
import com.example.fpapara.themoviedb.ui.moviedetails.model.MovieDetails;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class MovieDetailsPresenterTest {

    @Mock
    MovieDetailsService mMovieDetailsService;
    @Mock
    MovieDetailsContract mMovieDetailsContract;
    @Mock
    MovieDetails mMovieDetails;
    MovieDetailsPresenter mPresenter;
    EventBus mEventBus;

    @Before
    public void setUp() throws Exception {
        mMovieDetailsService = Mockito.mock(MovieDetailsService.class);
        mMovieDetailsContract = Mockito.mock(MovieDetailsContract.class);
        mMovieDetails = Mockito.mock(MovieDetails.class);
        mEventBus = EventBus.getDefault();
        mPresenter = new MovieDetailsPresenter(mMovieDetailsContract, mMovieDetailsService);
        mPresenter = Mockito.mock(MovieDetailsPresenter.class);
        when(mMovieDetails.getTitle()).thenReturn("Movie title");
        when(mMovieDetails.getTagline()).thenReturn("Movie TagLine");
        when(mMovieDetails.getHomepage()).thenReturn("Movie home page");
        when(mMovieDetails.getReleaseDate()).thenReturn("Movie release date");
        when(mMovieDetails.getStatus()).thenReturn("Movie status");
        when(mMovieDetails.getVoteAverage()).thenReturn(7.5);
    }

    @Test
    public void testLoadMovies() {
        Mockito.verify(mMovieDetailsService).loadMovieDetails();
    }

    @Test
    public void testOnDestroy() {
        Mockito.verify(mPresenter).onDestroy();
    }

    @Test
    public void testOnMessageEvent() throws Exception {
        mEventBus.postSticky(mMovieDetails);
        mEventBus.post(new MessageEvent(TmdbConstants.Extras.LOAD_SUCCESSFULLY_DETAILS));

        Mockito.verify(mMovieDetailsContract).hideAllItemShowLoading(Mockito.anyBoolean());
        Mockito.verify(mPresenter).onMessageEvent(Mockito.any(MessageEvent.class));
//        Mockito.verify(mMovieDetailsContract).setGenre(Mockito.anyString());
//        Mockito.verify(mMovieDetailsContract).setTagLine(Mockito.anyString());
//        Mockito.verify(mMovieDetailsContract).setHomePage(Mockito.anyString());
//        Mockito.verify(mMovieDetailsContract).setReleaseDate(Mockito.anyString());
//        Mockito.verify(mMovieDetailsContract).setStatus(Mockito.anyString());
//        Mockito.verify(mMovieDetailsContract).setVote(Mockito.anyString());
//        Mockito.verify(mMovieDetailsContract).setTitle(Mockito.anyString());
//        Mockito.verify(mMovieDetailsContract).getImageView();
//        Mockito.verify(mMovieDetailsContract).getContext();
    }
}