package com.example.fpapara.themoviedb.ui.login;

import android.content.Context;

import com.example.fpapara.themoviedb.TmdbConstants;
import com.example.fpapara.themoviedb.ui.login.api.LoginService;
import com.example.fpapara.themoviedb.util.model.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class LoginActivityPresenterTest {

    @Mock
    LoginActivityContract mLoginActivityContract;
    @Mock
    LoginService mLoginService;
    @Mock
    LoginActivityPresenter mLoginActivityPresenter;
    Context mContext;

    @Before
    public void setUp() throws Exception {

        mLoginActivityContract = Mockito.mock(LoginActivityContract.class);
        mLoginService = Mockito.mock(LoginService.class);
        mLoginActivityPresenter = Mockito.mock(LoginActivityPresenter.class);
        mContext = getInstrumentation().getTargetContext();
        //  mLoginActivityPresenter = new LoginActivityPresenter(mLoginActivityContract, mLoginService);
//        when(mLoginActivityContract.getUsername()).thenReturn("Username");
//        when(mLoginActivityContract.getPassword()).thenReturn("password");
//        when(mLoginActivityContract.getContext()).thenReturn(mContext);
//        when(TokenOperation.have(mContext)).thenReturn(true);
//        when(TokenOperation.get(mContext)).thenReturn("WeHaveNiceNewTokenForLoginHaveANiceDay");
//        when(TokenOperation.isValid(mContext)).thenReturn(true);
        mLoginActivityPresenter.login();
    }

    @Test
    public void testOnDestroy() throws Exception {
        Mockito.verify(mLoginActivityPresenter).onDestroy();
    }

    @Test
    public void testLogin() throws Exception {
        Mockito.verify(mLoginActivityPresenter).login();
    }

    @Test
    public void testOnMessageEvent() throws Exception {
        EventBus.getDefault().post(new MessageEvent(TmdbConstants.Extras.LOGIN_SUCCESS));
        Mockito.verify(mLoginActivityPresenter).onMessageEvent(Mockito.any(MessageEvent.class));
    }
}